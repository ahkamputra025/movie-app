const express = require('express');
const router = express.Router();

const controllers = require('../controllers/watchlist.controller');

router.get('/watchlist', controllers.getWatchlist);
router.get('/watchlist/:id', controllers.getWatchlistById);
router.post('/watchlist', controllers.createWatchlist);
router.put('/watchlist/:id', controllers.updateWatchlist);
router.delete('/watchlist/:id', controllers.deleteWatchlist);

module.exports = router;