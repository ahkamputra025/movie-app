const express = require('express');
const router = express.Router();

const controllers = require('../controllers/users.controller');
const middleware = require('../middleware/auth.middleware');

router.get('/users', controllers.getUsers);
router.get('/user/:id', controllers.getUserById);
// router.post('/users', middleware.Auth, controllers.createUser);
// router.put('/movie/:id', controllers.updateMovie);
// router.delete('/movie/:id', controllers.deleteMovie);

module.exports = router;