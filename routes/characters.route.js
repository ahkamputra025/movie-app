const express = require("express");
const router = express.Router();

const controllers = require("../controllers/characters.controller");

router.get("/characters", controllers.getCharacters);
router.get("/character/:id", controllers.getCharacter);
router.post("/character", controllers.createCharacter);
router.put("/character/:id", controllers.updateCharacter);
router.delete("/character/:id", controllers.deleteCharacter);

module.exports = router;
