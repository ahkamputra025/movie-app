const express = require("express");
const router = express.Router();

const controllers = require("../controllers/ratings.controller");

router.get("/ratings", controllers.getRatings);
router.get("/rating/:id", controllers.getRating);
router.post("/rating", controllers.createRating);
router.put("/rating/:id", controllers.updateRating);
router.delete("/rating/:id", controllers.deleteRating);

module.exports = router;