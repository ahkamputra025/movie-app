const express = require('express');
const router = express.Router();

const controllers = require('../controllers/reviews.controller');

router.get('/reviews', controllers.getReviews);
router.get('/review/:id', controllers.getReviewsById);
router.post('/reviews', controllers.createReview);
router.put('/review/:id', controllers.updateReview);
router.delete('/review/:id', controllers.deleteReview);

module.exports = router;