const router = require('express').Router();

const userController = require('../controllers/auth.controller');
const userMiddleware = require('../middleware/auth.middleware');

router.post('/register', userController.register);
router.post('/login', userMiddleware.validateLogin, userController.login);

module.exports = router;