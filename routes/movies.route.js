const express = require('express');
const router = express.Router();

const controllers = require('../controllers/movies.controller');

router.get('/movies', controllers.getMovies);
router.get('/movie/:id', controllers.getMovieById);
router.get('/moviesInclude/:id', controllers.getMoviesInclude);
router.post('/movies', controllers.createMovie);
router.put('/movie/:id', controllers.updateMovie);
router.delete('/movie/:id', controllers.deleteMovie);

module.exports = router;