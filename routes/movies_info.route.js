const express = require('express');
const router = express.Router();

const controllers = require('../controllers/movies_info.controller');

router.get('/movies_info', controllers.getMoviesInfo);
router.get('/movie_info/:id', controllers.getMovieInfoById);
router.post('/movies_info', controllers.createMovieInfo);
router.put('/movie_info/:id', controllers.updateMovieInfo);
router.delete('/movie_info/:id', controllers.deleteMovieInfo);

module.exports = router;