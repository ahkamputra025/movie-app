const express = require("express");
const router = express.Router();

const controllers = require("../controllers/actors.controller");

router.get("/actors", controllers.getActors);
router.get("/actor/:id", controllers.getActor);
router.post("/actor", controllers.createActor);
router.put("/actor/:id", controllers.updateActor);
router.delete("/actor/:id", controllers.deleteActor);

module.exports = router;
