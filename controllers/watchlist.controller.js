const { Watchlists } = require('../models');
const method = {};

method.getWatchlist = async (req, res) => {
    try {
        const data = await Watchlists.findAll({
            order: [
                ['id', 'DESC']
            ]
        });

        res.status(200).json({ data });
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getWatchlistById = async (req, res) => {
    try {
        const data = await Watchlists.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            res.status(200).json(data);
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.createWatchlist = async (req, res) => {
    try {
        const data = {...req.body};
        const result = await Watchlists.create(data);
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.updateWatchlist = async (req, res) => {
    try {
        const data = await Watchlists.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            await Watchlists.update(req.body, {
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Update success.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.deleteWatchlist = async (req, res) => {
    try {
        const cekData = await Watchlists.findOne({
            where: { id: req.params.id }
        });

        if(cekData){
            await Watchlists.destroy({
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Delete success.'});
        } else {
            res.status(401).json({message: 'id does not exist.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

module.exports = method;