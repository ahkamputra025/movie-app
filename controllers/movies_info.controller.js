const { MovieIf } = require('../models');
const method = {};

method.getMoviesInfo = async (req, res) => {
    try {
        const data = await MovieIf.findAll({
            order: [
                ['id', 'DESC']
            ]
        });
        
        res.status(200).json({ data });
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getMovieInfoById = async (req, res) => {
    try {
        const data = await MovieIf.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            res.status(200).json(data);
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.createMovieInfo = async (req, res) => {
    try {
        const data = {...req.body};
        const result = await MovieIf.create(data);
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.updateMovieInfo = async (req, res) => {
    try {
        const data = await MovieIf.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            await MovieIf.update(req.body, {
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Update success.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.deleteMovieInfo = async (req, res) => {
    try {
        const cekData = await MovieIf.findOne({
            where: { id: req.params.id }
        });
        
        if(cekData){
            await MovieIf.destroy({
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Delete success.'});
        } else {
            res.status(401).json({message: 'id does not exist.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

module.exports = method;