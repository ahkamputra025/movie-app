const { Ratings } = require("../models");
const method = {};

method.getRatings = async (req, res) => {
  try {
    const data = await Ratings.findAll({
      order: [["id", "DESC"]],
    });

    res.status(200).json({ data });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.getRating = async (req, res) => {
  try {
    const data = await Ratings.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      res.status(200).json(data);
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.createRating = async (req, res) => {
  try {
    const data = { ...req.body };
    const result = await Ratings.create(data);
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.updateRating = async (req, res) => {
  try {
    const data = await Ratings.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      await Ratings.update(req.body, {
        where: { id: req.params.id }
      });
      res.status(200).json({ message: "Update success." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.deleteRating = async (req, res) => {
  try {
    const cekData = await Ratings.findOne({
      where: { id: req.params.id },
    });

    if (cekData) {
      await Ratings.destroy({
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Delete success." });
    } else {
      res.status(401).json({ message: "id does not exist." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = method;
