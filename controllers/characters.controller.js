const { Characters, Actors } = require("../models");
const method = {};

method.getCharacters = async (req, res) => {
  try {
    const data = await Characters.findAll({
      include: Actors,
      // order: [["id", "DESC"]],
      
    });

    res.status(200).json({ data });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.getCharacter = async (req, res) => {
  try {
    const data = await Characters.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      res.status(200).json(data);
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.createCharacter = async (req, res) => {
  try {
    const data = { ...req.body };
    const result = await Characters.create(data);
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.updateCharacter = async (req, res) => {
  try {
    const data = await Characters.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      await Characters.update(req.body, {
        where: { id: req.params.id }
      });
      res.status(200).json({ message: "Update success." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.deleteCharacter = async (req, res) => {
  try {
    const cekData = await Characters.findOne({
      where: { id: req.params.id },
    });

    if (cekData) {
      await Characters.destroy({
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Delete success." });
    } else {
      res.status(401).json({ message: "id does not exist." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = method;
