const { User } = require('../models');
const bcrypt = require('jsonwebtoken');
const method = {};

method.getUsers = async (req, res) => {
    try {
        const data = await User.findAll({
            order: [
                ['id', 'DESC']
            ]
        });

        res.status(200).json({ data });
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getUserById = async (req, res) => {
    try {
        const data = await User.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            res.status(200).json(data);
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.createUser = async (req, res) => {
    try {
        const token = req.token;
        const email = token.email;

        const data = await User.findOne({

        });

        res.json(data);


        // res.send(token.email);
        // const data = {...req.body};
        // const result = await User.create(data);
        // res.status(200).json(result);
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

module.exports = method;