const { Movies, Characters, MovieIf } = require('../models');
const method = {};

method.getMovies = async (req, res) => {
    try {
        const {page, size} = req.query
        const data = await Movies.findAll({
            order: [
                ['id', 'DESC']
            ],
            limit: size,
            offset: page * size,
    
        });

        res.status(200).json({ data });
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getMoviesInclude = async (req, res) => {
    try {
        const data = await Movies.findOne({
            where: { id: req.params.id },
            order: [
                ['id', 'DESC']
            ],
            include: MovieIf
            
        });

        res.status(200).json({ data });
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getMovieById = async (req, res) => {
    try {
        const data = await Movies.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            res.status(200).json(data);
        }        
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.createMovie = async (req, res) => {
    try {
        const data = {...req.body};
        const result = await Movies.create(data);
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.updateMovie = async (req, res) => {
    try {
        const data = await Movies.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            await Movies.update(req.body, {
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Update success.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.deleteMovie = async (req, res) => {
    try {
        const cekData = await Movies.findOne({
            where: { id: req.params.id }
        });

        if(cekData){
            await Movies.destroy({
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Delete success.'});
        } else {
            res.status(401).json({message: 'id does not exist.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

module.exports = method;
