const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models');
const userController = {}

userController.register = async (req,res) => {
        
    try {
        const { email, password, name, user_img, role } = await req.body;
    
        const existingUser= await User.findOne({where : {email:email}})
        
        if (existingUser) {
            res.status(400).send({
                status: 400,
                message: 'email already registered'
            })
        } else {
            const newUser = await User.create({email, password:bcrypt.hashSync(password,10), name, user_img, role})
            
            const token = jwt.sign({email:newUser.email, role:newUser.role}, process.env.SECRET_KEY);
            res.header('Authorization', token);
            res.status(200).send({
                status: 200,
                message: "register success",
                data: token
            });
        }        
    } catch (error) {
        res.status(500).json({message: error.message});
    }
};

userController.login = async (req, res) => {
    let dtuser = await User.findOne({
        where: { email: req.body.email },

    });
    if (dtuser === null) {
        res.status(400).send({
            status: 400,
            message: "username does not exist",
            data: null,
        });
    }

    dtuser = dtuser.dataValues;
    delete dtuser.passsword;
    delete dtuser.updatedAt;

    const token = jwt.sign(dtuser, "secret_key");

    res.status(200).send({
        status: 200,
        message: "ok",
        data: {
            token,
        },
    });
};

module.exports = userController;