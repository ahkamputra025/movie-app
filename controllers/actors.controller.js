const { Characters, Actors } = require("../models");
const method = {};

method.getActors = async (req, res) => {
  try {
    const data = await Actors.findAll({
           include: Characters
    });

    res.status(200).json({ data });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.getActor = async (req, res) => {
  try {
    const data = await Actors.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      res.status(200).json(data);
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.createActor = async (req, res) => {
  try {
    const data = { ...req.body };
    const result = await Actors.create(data);
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.updateActor = async (req, res) => {
  try {
    const data = await Actors.findOne({
      where: { id: req.params.id },
    });

    if (!data) {
      res.status(401).json({ message: "id does not exist." });
    } else {
      await Actors.update(req.body, {
        where: { id: req.params.id }
      });
      res.status(200).json({ message: "Update success." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

method.deleteActor = async (req, res) => {
  try {
    const cekData = await Actors.findOne({
      where: { id: req.params.id },
    });

    if (cekData) {
      await Actors.destroy({
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Delete success." });
    } else {
      res.status(401).json({ message: "id does not exist." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = method;
