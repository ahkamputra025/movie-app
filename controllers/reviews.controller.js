const { Reviews } = require('../models');
const method = {};

method.getReviews = async (req, res) => {
    try {
        const {page, size} = req.query;
        const data = await Reviews.findAll({
            order: [
                ['id', 'DESC']
            ],
            limit: size,
            offset: page * size,
        });

        res.status(200).json({data});
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.getReviewsById = async (req, res) => {
    try {
        const data = await Reviews.findOne({
            where: { id: req.params.id }
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            res.status(200).json(data);
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.createReview = async (req, res) => {
    try {
        const data = {...req.body};
        const result = await Reviews.create(data);
        res.status(200).json(data);
        res.status
    } catch (error) {
        res.status(500).json({message: error,message});
    }
}

method.updateReview = async (req, res) => {
    try {
        const data = await Reviews.findOne({
            where: { id: req.params.id}
        });

        if(!data){
            res.status(401).json({message: 'id does not exist.'});
        } else {
            await Reviews.update(req.body, {
                where: { id: req.params.id }
            })
            res.status(200).json({message: 'Update success.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

method.deleteReview = async (req, res) => {
    try {
        const cekData = await Reviews.findOne({
            where: { id: req.params.id }
        });

        if(cekData){
            await Reviews.destroy({
                where: { id: req.params.id }
            });
            res.status(200).json({message: 'Delete success.'});
        } else {
            res.status(401).json({message: 'id does not exist.'});
        }

    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

module.exports = method;