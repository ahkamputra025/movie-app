const express = require('express');
const { sequelize } = require('./models');
const cors = require ('cors')
require('dotenv').config();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const authRoutes = require('./routes/auth.route');
const userRoutes = require('./routes/users.route');
const movieRoutes = require('./routes/movies.route');
const moviesInfoRoutes = require('./routes/movies_info.route');
const reviewsRoutes = require('./routes/reviews.route');
const watchlistRoutes = require('./routes/watchlist.route');
const actorRoutes = require("./routes/actors.route");
const characterRoutes = require("./routes/characters.route");
const ratingRoutes = require("./routes/ratings.route");

app.use('/api', authRoutes);
app.use('/api', userRoutes);
app.use('/api', movieRoutes);
app.use('/api', moviesInfoRoutes);
app.use('/api', reviewsRoutes);
app.use('/api', watchlistRoutes);
app.use("/api", actorRoutes);
app.use("/api", characterRoutes);
app.use("/api", ratingRoutes);

app.all('*', (req, res) => {
    res.send(`Route doesn't exist.`);
});

const port = process.env.PORT;
app.listen(port, async () => {
    console.log(`Server up on http://localhost:${port}`);
    await sequelize.authenticate();
    console.log('Database connected');
});