"use strict";
const { Model } = require("sequelize");
const movies_infoModel = require("./movies_info.model");
module.exports = (sequelize, DataTypes) => {
  class characters extends Model {
    static associate( {Actors, Movies} ) {
      characters.belongsTo(Actors, { foreignKey: 'actor_id' }),
      characters.belongsTo(Movies, {foreignKey: 'movie_id'})
    }
  }
  characters.init(
    {
      movie_id: DataTypes.INTEGER,
      actor_id: DataTypes.INTEGER,
      character_name: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "characters",
      modelName: "Characters",
    }
  );
  return characters;
};
