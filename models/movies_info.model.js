'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MovieIf extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Movies}) {
      // define association here
      MovieIf.belongsTo(Movies, {foreignKey: 'id'})
    }
  };
  MovieIf.init({
    release_date: DataTypes.DATE,
    director: DataTypes.STRING,
    featured_song: DataTypes.STRING,
    budget: DataTypes.STRING,
    movie_id: DataTypes.INTEGER,
  }, {
    sequelize,
    tableName: 'movies_info',
    modelName: 'MovieIf',
  });
  return MovieIf;
};