'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Watchlists extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Movies}) {
      // define association here
      Watchlists.belongsTo(Movies, {foreignKey:'movie_id'})
    }
  };
  Watchlists.init({
    movie_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
  }, {
    sequelize,
    tableName: 'watchlists',
    modelName: 'Watchlists',
  });
  return Watchlists;
};