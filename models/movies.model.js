'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Characters, Watchlists, MovieIf}) {
      // define association here
      Movies.hasMany(Characters, {foreignKey: 'id'}),
      Movies.hasMany(Watchlists, {foreignKey: 'id'}),
      Movies.hasOne(MovieIf, {foreignKey: 'id'})
      
    }
  };
  Movies.init({
    title: DataTypes.STRING,
    synopsis: DataTypes.TEXT,
    category: DataTypes.STRING,
    url_trailer: DataTypes.STRING,
    url_poster: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'movies',
    modelName: 'Movies',
  });
  return Movies;
};