"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class actors extends Model {
    static associate({ Characters }) {
      actors.hasMany(Characters, {foreignKey:'actor_id'});
    }
  }
  actors.init(
    {
      name: DataTypes.STRING,
      url_img: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "actors",
      modelName: "Actors",
    }
  );
  return actors;
};